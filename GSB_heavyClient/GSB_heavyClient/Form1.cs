﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace GSB_heavyClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //new WebRequestGet = test;
        }

        private void WebRequestAPI(string url)
        {
            // Create a request for the URL. 		
            WebRequest request = WebRequest.Create(url);
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // Display the status.
            Console.WriteLine(response.StatusDescription);
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content
            string responseFromServer = reader.ReadToEnd();
            object responseObject = Convert.ChangeType(responseFromServer, typeof(Object));
            Console.WriteLine(responseObject);
            // Display the content.
            /*for (int i = 0; i < responseObject.GetType().GetFields().Length; i++)
            {
                string test = responseObject.GetType().GetFields()[i].GetValue(responseObject).ToString();
                Console.WriteLine(test);
            }*/

            // Cleanup the streams and the response.
            reader.Close();
            dataStream.Close();
            response.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WebRequestAPI("http://172.21.4.124/PPE_API_HR/visitor");
        }
    }
}
